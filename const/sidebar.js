export default {
    MENU: [
        { index: "1", title: "Base64",             link: "base64" },
        { index: "2", title: "簡易テンプレート",   link: "template" },
        { index: "3", title: "連番メーカー",       link: "sequence" },
        { index: "4", title: "らくらく連続置換",   link: "replacer" },
        { index: "5", title: "横にくっつけるやつ", link: "paste" },
        { index: "6", title: "ファイル分割 & DL",  link: "separator" },
        { index: "7", title: "集合演算",           link: "setops" },
        { index: "8", title: "フォーマット変換器", link: "convformat" },
    ]
}