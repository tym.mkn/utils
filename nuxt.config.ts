// https://nuxt.com/docs/api/configuration/nuxt-config
const lifecycle = process.env.npm_lifecycle_event

export default defineNuxtConfig({
    css: ['~/assets/css/elementPlus.scss'],
    app: {
        baseURL: "/"
    },
    build: {
        transpile: lifecycle === 'build' ? ['element-plus'] : []
    },
    vite:{
        server: {
            watch: {
                usePolling: true,
            },
        },
    },
})
